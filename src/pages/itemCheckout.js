import Image from "next/image";
import { StarIcon } from '@heroicons/react/solid'
import Currency from 'react-currency-formatter'
import { addBasket , removeBasket} from '../redux/actions/basket'
import { connect } from "react-redux";

function itemCheckout({ id, title, price, description, category, image, rating, hasPrime, addBasket , removeBasket}) {
    const addToBasket = ()=>{
        const product =  {
            id,
            title,
            price,
            rating,
            description,
            category,
            image,
            hasPrime
        }
        addBasket(product)
    }

    const removeFromBasket = ()=>{
        removeBasket(id)
    }
    return (
        <div className="grid grid-cols-5">
            <Image
                src={image}
                width={200}
                height={200}
                objectFit="contain"
            />
            <div className="col-span-3 mx-5">
                <p>{title}</p>
                <div className="flex">
                    {Array(rating)
                        .fill()
                        .map((_, i) => (
                            <StarIcon className="h-5 text-yellow-500" />
                        ))}
                </div>
                <p className="text-sm line-clamp-3">{description}</p>
                <Currency quantity={price} currency="GBP" />
                {
                    hasPrime && (
                        <div className="flex items-center space-x-2 mt-5">
                        <img className="w-12" src="https://links.papareact.com/fdw" loading="lazy" alt="" />
                        <p className="text-xs text-gray-500">FREE Next day delivery</p>
                    </div>
                    )
                }
            </div>
            <div className="flex flex-col space-y-2 my-auto justify-self-end">
             <button className="button" onClick={()=>addToBasket()}>Add to basket</button>
             <button className="button" onClick={()=>removeFromBasket()}>Remove from basket</button>
            </div>
        </div>
    )
}
const mapStateToProps = (state) => ({
    items: state.basket.items
})
export default connect(mapStateToProps, {addBasket, removeBasket})(itemCheckout)
