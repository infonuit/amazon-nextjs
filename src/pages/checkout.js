import { connect } from "react-redux"
import Image from "next/image"
import Header from "../components/Header"
import ItemCheckout from './itemCheckout'
import { useSession } from 'next-auth/client'
import Currency from 'react-currency-formatter'
import { loadStripe } from '@stripe/stripe-js'
import axios from 'axios'
const stripePromise  = loadStripe(process.env.stripe_public_key)


function Checkout({ items }) {
  /* get session google */
  const [session] = useSession()
  /* total items */
  const total = items.reduce((total, item) => total + item.price, 0)

  /* stripe session  */
  const createCheckoutSession = async() => {
     const stripe = await stripePromise;
     
     const checkoutSession = await axios.post('/api/create-checkout-session', {
       items: items,
       email: session.user.email
      }
     )
     // When the customer clicks on the button, redirect them to Checkout.
    const result = await stripe.redirectToCheckout({
      sessionId: checkoutSession.data.id,
    });

    if (result.error) {
      alert(result.error.message)
    }
  }
  return (
    <div className="bg-gray-100">
      <Header />
      <main className="lg:flex max-w-screen-2xl mx-auto">
        {/* left side */}
        <div className="flex-grow m-5">
          <Image
            src="https://links.papareact.com/ikj"
            width={1024}
            height={250}
            objectFit="contain"
          />
          <div className="flex flex-col p-5 space-y-10 bg-white">
            <h1 className="text-3xl border-b pb-4">
              {items.length === 0 ? 'Your shopping basket is empty' : 'shopping basket'}
            </h1>

            {
              items.map((item, i) => (
                <ItemCheckout
                  key={i}
                  id={item.id}
                  title={item.title}
                  price={item.price}
                  description={item.description}
                  category={item.category}
                  image={item.image}
                  rating={item.rating}
                  hasPrime={item.hasPrime}
                />
              ))
            }
          </div>
        </div>
        {/* right side */}
        <div>
          {
            items.length > 0 && (
              <div className="flex flex-col bg-white p-10 shadow-md">
                <>
                  <h2 className="whitespace-nowrap">Subtotal ({items.length}: item)</h2>
                  <span className="font-bold">
                    <Currency quantity={total} currency="GBP" />
                  </span>
                  <span>

                  </span>
                  <button
                    role="link"
                    onClick={()=>createCheckoutSession()}
                    disabled={!session}
                    className={`button mt-2 ${!session && "from-gray-100 to-gray-500 border-gray-200 text-gray-500 cursor-not-allowed"}`}>
                    {
                      !session ? 'signin to checkout' : 'proceed to payment'
                    }
                  </button>
                </>
              </div>
            )
          }
        </div>



      </main>
    </div>
  )
}
const mapStateToProps = (state) => ({
  items: state.basket.items
})
export default connect(mapStateToProps)(Checkout);
