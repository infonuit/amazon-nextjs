import { ADD_TO_BASKET, REMOVE_FROM_BASKET } from "../types";

const initialState = {
    items: []
}
function basket(state = initialState, action){
    switch (action.type) {
        case ADD_TO_BASKET:
            return  {
                ...state,
                items: [...state.items, action.payload]
            }

        case REMOVE_FROM_BASKET:
            return   {
                ...state,
                items: state.items.filter(basketItem=>basketItem.id !== action.payload)
            }
            
            
        default:
            return state
    }
}

export default basket;