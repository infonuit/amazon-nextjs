import { ADD_TO_BASKET, REMOVE_FROM_BASKET } from "../types"

export const addBasket = (product)=> dispatch=>{
    return dispatch({
        type: ADD_TO_BASKET,
        payload: product
    })
}

export const removeBasket = (id)=> dispatch=>{
    return dispatch({
        type: REMOVE_FROM_BASKET,
        payload: id
    })
}