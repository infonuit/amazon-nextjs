import Image from 'next/image'
import { useRouter } from 'next/router'
import { MenuIcon, SearchIcon, ShoppingCartIcon } from '@heroicons/react/outline';/* outline for 20X20  and solid for 24X24*/
import { signIn, signOut, useSession } from 'next-auth/client'
import { connect } from 'react-redux';


function Header({items}) {
   const [session] = useSession()
   const router = useRouter();


   
    return (
        <div>
            <header>
                {/* top header */}
                <div className="flex items-center bg-amazon_blue p-1 flex-grow py-2">
                  <div className="mt-2 flex items-center flex-grow sm:flex-grow-0">
                      
                      <Image 
                      onClick={()=>router.push('/')}
                      src="https://links.papareact.com/f90" 
                      width={150} 
                      height={50}
                      objectFit="contain"
                      className="cursor-pointer"
                      />
                      
                  </div>
                  {/* search */}
                  <div className="hidden sm:flex items-center h-12  rounded-md bg-yellow-400 hover:bg-yellow-500 flex-grow">
                      <input type="text" className="p-2 h-full w-6 flex-grow rounded-l-md focus:outline-none"/>
                      <SearchIcon className="h-12 p-4"/>
                  </div>
                  {/* right */}
                  <div className="text-white items-center flex text-xs space-x-6 m-6 whitespace-nowrap">
                      <div className="link" onClick={!session ? signIn: signOut}>
                          <p>{session ? session.user.name: 'connect please'}</p>
                          <p className="font-extrabold md:text-sm">Comptes et listes</p>
                      </div>
                      <div className="link">
                          <p>Retours</p>
                          <p className="font-extrabold md:text-sm">et Commandes</p>
                      </div>
                      <div className="relative flex items-center link" onClick={()=>router.push('/checkout')}>
                          <span className="absolute top-0 right-0 md:right-10 h-4 w-4 bg-yellow-400 text-center rounded-full text-black font-bold">{items.length}</span>
                          <ShoppingCartIcon className="h-10"/>
                          <p className="hidden md:inline font-extrabold md:text-sm mt-2">
                              Panier
                          </p>
                      </div>
                  </div>
                </div>
                
                <div className="flex items-center bg-amazon_blue-light space-x-3 p-2 pl-6 text-white cursor-pointer lg:text-sm">
                    <p className="flex items-center link">
                        <MenuIcon className="h-6"/>
                        Toutes
                    </p>
                    <p className="link">
                       Meilleures Ventes
                    </p>
                    <p className="link">
                    Amazon Basics
                    </p>
                    <p className="link">
                    Dernières Nouveautés
                    </p>
                    <p className="link hidden lg:inline-flex">
                        Prime
                    </p>
                    <p className="link hidden lg:inline-flex">
                        Service Client
                    </p>
                    <p className="link hidden lg:inline-flex">
                        Livres
                    </p>
                    <p className="link hidden lg:inline-flex">
                        Cuisine & maison
                    </p>
                    <p className="link hidden lg:inline-flex">
                        Hight-Tech
                    </p>
                    <p className="link hidden lg:inline-flex">
                        Prime Video
                    </p>
                </div>
            </header>
        </div>
    )
}
const mapStateToProps = (state) => ({
    items: state.basket.items
  })
export default connect(mapStateToProps) (Header)
