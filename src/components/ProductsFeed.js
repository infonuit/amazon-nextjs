import Product from "./Product"

function ProductsFeed({ products }) {
    return (
        <div className="grid grid-flow-row-dense md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 md:-mt-52">
           {
               products.slice(0,4).map((product, i)=>(
                   <Product  
                   key={i}
                   id={product.id}
                   title={product.title}
                   price={product.price}
                   description={product.description}
                   category={product.category}
                   image={product.image}
                   />
               ))
           }
           <div className="md:col-span-2">
           {
               products.slice(4,5).map((product, i)=>(
                   <Product  
                   key={i}
                   id={product.id}
                   title={product.title}
                   price={product.price}
                   description={product.description}
                   category={product.category}
                   image={product.image}
                   />
               ))
           }
           </div>
           
           {
               products.slice(5,products.length).map((product, i)=>(
                   <Product  
                   id={i}
                   title={product.title}
                   price={product.price}
                   description={product.description}
                   category={product.category}
                   image={product.image}
                   />
               ))
           }
           
        </div>
    )
}

export default ProductsFeed
